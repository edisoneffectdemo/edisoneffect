package mayragc.edisoneffectdemo;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList<Supplier> suppliers;

    private ListView supplierList;
    private SupplierAdapter supplierAdapter;

    private TextView name;
    private TextView info;

    RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        name = findViewById(R.id.name);
        info = findViewById(R.id.phone);

        suppliers = new ArrayList<>();
        supplierAdapter = new SupplierAdapter(this, suppliers);

        supplierList = findViewById(R.id.supplier_list);
        supplierList.setAdapter(supplierAdapter);

        requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, "https://www.levelgas.com/Tr4ck3r/testInterview.php", null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject userJsonObject = response.getJSONObject("user");
                            String user_name = userJsonObject.getString("name");
                            String user_lastname = userJsonObject.getString("lastName");
                            String user_telephone = userJsonObject.getString("telephone");
                            String user_celphone = userJsonObject.getString("celphone");

                            name.setText(user_name + " " + user_lastname);
                            info.setText(user_telephone + " " + user_celphone);
                            JSONArray suppliersJsonArray = response.getJSONArray("suppliers");
                            for(int i = 0; i < suppliersJsonArray.length(); i++) {
                                JSONObject supplierJson = suppliersJsonArray.getJSONObject(i);
                                JSONObject addressObject = supplierJson.getJSONObject("address");
                                String id = Integer.toString(supplierJson.getInt("id"));
                                String name = supplierJson.getString("name");
                                String rating = supplierJson.getString("rating");
                                String imageUrl = supplierJson.getString("image");
                                String street = addressObject.getString("street");
                                String colony = addressObject.getString("colony");
                                suppliers.add(new Supplier(id, name, rating, imageUrl,new Address(street,colony)));
                            }
                            supplierAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {

                            Log.e("EF DEMO", Log.getStackTraceString(e));
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error

                    }
                });
        requestQueue.add(jsonObjectRequest);
    }

    public class SupplierAdapter extends ArrayAdapter<Supplier> {

        private Context context;
        private ArrayList<Supplier> suppliers;


        public SupplierAdapter(@NonNull Context context, @NonNull ArrayList<Supplier> suppliers) {
            super(context, 0, suppliers);
            this.context = context;
            this.suppliers = suppliers;
        }

        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            View listItem = convertView;
            if (listItem == null)
                listItem = LayoutInflater.from(context).inflate(R.layout.adapter_suppliers, parent, false);

            Supplier currentSupplier = suppliers.get(position);

            if (currentSupplier != null) {
                TextView supplierName = listItem.findViewById(R.id.supplier_name);
                supplierName.setText(currentSupplier.getName());
                TextView supplierRating = listItem.findViewById(R.id.supplier_rating);
                supplierRating.setText(currentSupplier.getRating());
                TextView supplierAddress = listItem.findViewById(R.id.supplier_street);
                supplierAddress.setText(currentSupplier.getAddress().getStreet());
                TextView supplierColony = listItem.findViewById(R.id.supplier_colony);
                supplierColony.setText(currentSupplier.getAddress().getColony());
                ImageView image = listItem.findViewById(R.id.supplier_image_url);
                new DownloadImageTask(image).execute(currentSupplier.getImageUrl());
            }

            return listItem;
        }
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView imageView;
        public DownloadImageTask(ImageView imageView) {
            this.imageView = imageView;
        }

        protected Bitmap doInBackground(String... params) {
            String url = params[0];
            Bitmap bitmap = null;
            try {
                InputStream inputStream = new java.net.URL(url).openStream();
                bitmap = BitmapFactory.decodeStream(inputStream);
            } catch (Exception e) {
                Log.e("EF DEMO", Log.getStackTraceString(e));
            }
            return bitmap;
        }
        protected void onPostExecute(Bitmap result) {
            imageView.setImageBitmap(result);
        }
    }


}
