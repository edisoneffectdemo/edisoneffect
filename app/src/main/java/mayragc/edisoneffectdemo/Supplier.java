package mayragc.edisoneffectdemo;

public class Supplier {
    private String id;
    private String name;
    private String rating;
    private String imageUrl;
    private Address address;

    public Supplier(String id, String name, String rating, String imageUrl,Address address) {
        this.id = id;
        this.name = name;
        this.rating = rating;
        this.imageUrl = imageUrl;
        this.address = address;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getRating() {
        return rating;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public Address getAddress() {
        return address;
    }
}
