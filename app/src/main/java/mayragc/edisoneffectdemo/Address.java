package mayragc.edisoneffectdemo;

public class Address {
    private String street;
    private String colony;

    public Address(String street, String colony) {
        this.street = street;
        this.colony = colony;
    }

    public String getStreet() {
        return street;
    }

    public String getColony() {
        return colony;
    }
}
